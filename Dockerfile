from jenkinsci/blueocean:1.19.0
 
USER root
RUN apk update
RUN apk add docker
RUN addgroup jenkins docker

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.16.1/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl

USER jenkins